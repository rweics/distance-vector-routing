from sim.api import *
from sim.basics import *

'''
Create your distance vector router in this file.
'''
class DVRouter (Entity):
    def __init__(self):
        self.distance_vector = dict() # Key - tuple (neighbor, destination); Value - tuple (shortest distance from neighbor to destination)
        self.neighbors = dict() # Key - neighbor; Value - tuple (edge weight, port)
        self.routing_table = dict() # Key - destination; Value - next stop


    def handle_rx (self, packet, port):
        # Add your code here!
        if isinstance(packet, RoutingUpdate):
            self.handle_RoutingUpdate(packet, port)
        elif isinstance(packet, DiscoveryPacket):
            self.handle_DiscoveryPacket(packet, port)
        else:
            self.handle_OtherPackets(packet, port)


    def handle_RoutingUpdate(self, packet, port):
        update_existing_neighbors = RoutingUpdate()
        for dst in packet.all_dests():
            distance = packet.get_distance(dst)
            # if the destination does not exist in routing table yet, update to make packet sender the preferred "next_stop"
            self.distance_vector[(packet.src, dst)] = distance
            if dst != self and dst not in self.routing_table:
                self.routing_table[dst] = packet.src
                # add entry into update_existing_neighbors because we've never seen it before and need to propagate to other nodes
                update_existing_neighbors.add_destination(dst, distance + self.neighbors[packet.src][0])
            elif dst != self:
                # update the distance_vector and routing_table if necessary, take both distance and port number into account
                old_entry = self.routing_table[dst]
                curr_min = self.distance_vector[(old_entry, dst)]+self.neighbors[self.routing_table[dst]][0]
                curr_port = self.neighbors[self.routing_table[dst]][1]
                best_neighbor = old_entry
                for n in self.neighbors.keys():
                    if (n, dst) in self.distance_vector:
                        temp_dist = self.distance_vector[(n, dst)]+self.neighbors[n][0]
                        temp_port = self.neighbors[n][1]
                        if temp_dist < curr_min or (temp_dist == curr_min and temp_port < curr_port):
                            curr_min = temp_dist
                            curr_port = temp_port
                            best_neighbor = n
                if curr_min != self.distance_vector[(old_entry, dst)]+self.neighbors[self.routing_table[dst]][0]:
                    update_existing_neighbors.add_destination(dst, curr_min)
                    self.routing_table[dst] = best_neighbor
                if old_entry != best_neighbor:
                    self.routing_table[dst] = best_neighbor
        if len(update_existing_neighbors.all_dests()):
            self.send(update_existing_neighbors, port, True)


    def handle_DiscoveryPacket(self, packet, port):

        if packet.is_link_up:

            # NOTE: having a new link (self, NODE) does not necessarily mean that the direct connection is shortest path #

            distance = packet.latency

            # pass distance_vector entires corresponding to my routing table to new neighbor
            update_new_neighbor = RoutingUpdate()
            update_new_neighbor.dst = packet.src
            for dst in self.routing_table:
                via = self.routing_table[dst]
                update_new_neighbor.add_destination(dst, self.distance_vector[(via,dst)]+self.neighbors[via][0])
            self.send(update_new_neighbor, port, False)

            # adding new neighbor to self.neighbors
            self.neighbors[packet.src] = (distance, port)

            # construct a fake incoming RoutingUpdate and let handle_RoutingUpdate to deal with updating tasks
            # because current node might have been already connected to the new neighbor via some shorter paths
            # such that routing_table does not need to be updated and existing neighbors do not need to be bothered
            update_existing_neighbors = RoutingUpdate()
            update_existing_neighbors.src = packet.src
            update_existing_neighbors.dst = self
            update_existing_neighbors.add_destination(packet.src, 0)
            self.handle_RoutingUpdate(update_existing_neighbors, port)

        elif not packet.is_link_up:
            update_existing_neighbors = RoutingUpdate()
            # remove the node from neighbors list
            del self.neighbors[packet.src]
            # update and recompute my distance_vector
            for key in self.distance_vector.keys():
                if key[0] == packet.src:
                    del self.distance_vector[key]
            # update my routing table and inform my neighbors about changes
            for dst in self.routing_table.keys():
                if self.routing_table[dst] == packet.src:
                    self.routing_table[dst] = None
                    curr_min = float('inf')
                    curr_port = float('inf')
                    for n in self.neighbors.keys():
                        if (n, dst) in self.distance_vector:
                            temp_dist = self.distance_vector[(n, dst)]+self.neighbors[n][0]
                            temp_port = self.neighbors[n][1]
                            if temp_dist < curr_min or (temp_dist == curr_min and temp_port < curr_port):
                                curr_min = temp_dist
                                curr_port = temp_port
                                self.routing_table[dst] = n
                    if not self.routing_table[dst]:
                        del self.routing_table[dst]
                    else:
                        via = self.routing_table[dst]
                    update_existing_neighbors.add_destination(dst, curr_min)
            # inform all existing neighbors that the link is down according to updated distance_vector
            self.send(update_existing_neighbors, port, True)


    def handle_OtherPackets(self, packet, port):
        dst = packet.dst
        if dst in self.routing_table:
            distance = self.distance_vector[(self.routing_table[dst], dst)] + self.neighbors[self.routing_table[dst]][0]
            if distance <= 50:
                next_stop = self.routing_table[packet.dst]
                # forward the packet
                self.send(packet, self.neighbors[next_stop][1], False)

