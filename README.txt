1. Randy Wei
   Hye Min Cho

2. When we were first implementing the router, we thought the port is not
   necessarity connected with the Entities that are physically connected
   to the router, but misunderstood that those are ports similar to port 80
   in http://localhost:80. Therefore, we did not know how to send out the 
   packet to the routers that we are connected with. Later on we realized that
   the ports correspond to each connection to our neighbors.

3. We can improve the security of the router by keeping track of the frequency
   of packets being sent from certain source to certain destination. If it is
   too frequent, we will consider it as an attack and alert the destination.
   If the destination wishes so, we will then drop packets going from src to
   that destination afterwards.

4. Our code can handle link weights and incremental updates.

   When dealing with link weights, as opposed to hop counts, an important additional
   consideration is that, the direct connection between current router and a new 
   neighbor is not necessarily the shortest path any more. The new neighbor might be
   previously connected to current node by other paths with shorter distance, so 
   propagation to the whole network might not be necessary, since the routing table 
   might not change.
   Consider an existing network A-B-C-D where each edge has weight 1, if a new link
   A-D is added with weight 10, we do not update the distance vector nor do we
   inform everyone else in the network because shortest path from A to D is still
   A-B-C-D with total distance 3, while path A-D has total distance 10.
   
   For incremental updates we only send out updates to neighbors when there
   are changes in routing_table, and we only include the updated entries in the
   UpdatePacket. If distances change but shortest paths outcomes do not change,
   we do not have to inform other nodes. We almost never send out a complete copy
   of distance vector unless really necessary.